import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_city_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city},{state}";
    headers = {
        "Authorization": PEXELS_API_KEY
    }
    response = requests.get(url, headers=headers)
    data = json.loads(response.content)
    return data["photos"][0]["src"]["original"]


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city and state
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},&appid={OPEN_WEATHER_API_KEY}"
    # Make the GET request
    response = requests.get(geo_url)
    # Parse the JSON response
    coord = json.loads(response.content)
    # Get the latitude and longitude from the response
    lat = coord[0]["lat"]
    lon = coord[0]["lon"]
    # print(lat)
    # print(lon)
    # return lat, lon
    # Create the URL for the current weather API with the latitude
    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}8&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    response = requests.get(weather_url)
    # Parse the JSON response
    data = json.loads(response.content)
    # Get the main temperature and the weather's description and put them in a dictionary
    description = data["weather"][0]["description"]
    temp = data["main"]["temp"]
    return temp, description
